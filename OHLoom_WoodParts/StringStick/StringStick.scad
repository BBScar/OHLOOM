/*File Info--------------------------------------------------------------------
File Name: StringStick.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


length=560;
width=22;
height=6;
hole=2.5; //2.5 default - use 7 only for marking the holes with CNC
dist1=50;
dist2=206;
export_dxf=false;

if(export_dxf)
{
projection()
{
    difference()
    {
        hull()
        {
            translate([width/2,0,0])
            cylinder(h=height,d=width);
            translate([length-width/2,0,0])
            cylinder(h=height,d=width);
        }
        translate([dist1,0,0])
        cylinder(h=height,d=hole);
        translate([length-dist1,0,0])
        cylinder(h=height,d=hole);
        translate([dist2,0,0])
        cylinder(h=height,d=hole);
        translate([length-dist2,0,0])
        cylinder(h=height,d=hole);
        }
    }
}
else
{
    difference()
    {
        hull()
        {
            translate([width/2,0,0])
            cylinder(h=height,d=width);
            translate([length-width/2,0,0])
            cylinder(h=height,d=width);
        }
        translate([dist1,0,0])
        cylinder(h=height,d=hole);
        translate([length-dist1,0,0])
        cylinder(h=height,d=hole);
        translate([dist2,0,0])
        cylinder(h=height,d=hole);
        translate([length-dist2,0,0])
        cylinder(h=height,d=hole);
        
    }
}