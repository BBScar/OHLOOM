# OHLOOM - Montageanleitung
|

![](Assembly_Guide.jpg)

|

## Index


* Montageteile

 1.1. [Metallteile](#metalparts)

 1.2. [3D Druckteile](#printparts)

 1.3. [CNC Teile](#cncparts)

 1.4. [Andere Teile](#otherparts)

 * Montagevorbereitung

 2.1. [Colorful Wood Protection Film](#protection)

* [Montage](#assembly) 

|


# 1. Montageteile

## 1.1. Metallteile <a name="metalparts">



|![](Parts_Metal_1.jpg)|![](Parts_Metal_2.jpg)|![](Parts_Metal_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Gewindebolzen M8 x 180 mm |4 x M8 Muttern|2 x M8 Muttern mit Gummiring|
|![](Parts_Metal_4.jpg)|![](Parts_Metal_5.jpg)|
|4 x M6 Muttern |4 x M6 Zylinderschrauben |
|![](Parts_Metal_6.jpg)|![](Parts_Metal_7.jpg)|![](Parts_Metal_8.jpg)|
|8 x W60x4 Holzschrauben|4 x W35x4 Holzschrauben|16 x W12x4 Holzschrauben|


|

## 1.2. 3D Druckteile <a name="printparts">


|![](Parts_Print_1.jpg)|![](Parts_Print_2.jpg)|![](Parts_Print_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Sperrrad|2 x Klemmenring|5 x Kammmodul|
|![](Parts_Print_4.jpg)|![](Parts_Print_5.jpg)|![](Parts_Print_6.jpg)|
|2 x Sperrkeil| 12 x Schraubenfuss mit| 4 x Schraubenfuss ohne  |
|![](Parts_Print_7.jpg)|
|2 Achtkantwellenelemente (4 pro Einheit)|


|

## 1.3. CNC Teile (Holz) <a name="cncparts">

|![](Parts_CNC_1.jpg)|![](Parts_CNC_2.jpg)|![](Parts_CNC_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Kammhalter|2 x Seitenteil|2 x Querteil|
|![](Parts_CNC_4.jpg)|![](Parts_CNC_5.jpg)|![](Parts_CNC_6.jpg)|
|2 x Einschnittleiste|2 x Schnurleiste|1 x Schiff|


|

## 1.4. Andere Teile <a name="otherparts">

|![](Parts_Other_1.jpg)|![](Parts_Other_2.jpg)|
|:---------:|:------------:|
|2 x Holzwelle| ca. 3 m Schnur|

|

# 2. Montagevorbereitung

## 2.1. Farbiger Schutzfilm <a name="protection">


|![](Assembly_Preparation_1.jpg)                                                      |
|:-----------------------------------------------------------------------------------:|
| 1. Man braucht Colorpigmente, Leimoel, ein leeres Glass und Pinsel                                                                                                             |
|![](Assembly_Preparation_2.jpg)                                                      |
| 2. Zuerst fuelle die Pigmente ins Glas (ca. 10-20 grams).                                                                                                                      |
|![](Assembly_Preparation_3.jpg)                                                      |
| 3. Fuelle als naechstes 8-10 mal soviel Oel wie Pigmente ins Glass und ruehre kraeftig.                                                                                        |
|![](Assembly_Preparation_4.jpg)                                                      |
| 4.                                                                                    Ruehr solang die Pigmente bis sie gut vermischt sind und keine Kluempchen mehr aufweisen.|
|![](Assembly_Preparation_5.jpg)                                                      |
| 5. Die Holzoberflaeche sollte sauber und trocken sein.                              |
| 6. Trage die Farbe in 1-3  Schichten auf, bis es das gewuenschte  Aussehen aufweist.|
| 7. Lass es ueber Nacht trocknen.                                                                                                                                               |

|

# 3. Montage <a name="assembly">


|![](Assembly_1.jpg)![](Assembly_2.jpg)                               |
|:---------:|
| 1.|
|![](Assembly_3.jpg)![](Assembly_4.jpg)                               |
| 2.|
|![](Assembly_5.jpg)![](Assembly_6.jpg)                               |
| 3.|
|![](Assembly_7.jpg)![](Assembly_8.jpg)                               |
| 4.|
|![](Assembly_9.jpg)![](Assembly_10.jpg)                              |
| 5.|
|![](Assembly_11.jpg)![](Assembly_12.jpg)                             |
| 6.|
|![](Assembly_13.jpg)![](Assembly_14.jpg)![](Assembly_15.jpg)         |
| 7.|
|![](Assembly_16.jpg)![](Assembly_17.jpg)![](Assembly_18.jpg)         |
| 8.|
|![](Assembly_19.jpg)![](Assembly_20.jpg)![](Assembly_21.jpg)         |
| 9.|
|![](Assembly_22.jpg)![](Assembly_23.jpg)                             |
| 10.|
|![](Assembly_24.jpg)|
| 11.|
|![](Assembly_25.jpg)|
| Voila! Dein erster OHLOOM Webstuhl ist zusammengebaut. Glueckwunsch!|