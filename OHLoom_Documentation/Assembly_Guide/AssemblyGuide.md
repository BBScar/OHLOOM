# OHLOOM - Assembly Guide
|

![](Assembly_Guide.jpg)

|

## Index


1. Assembly Parts

 1.1. [Metal Parts](#metalparts)

 1.2. [3D Print Parts](#printparts)

 1.3. [CNC Parts](#cncparts)

 1.4. [Other Parts](#otherparts)

2. Assembly Preparation

 2.1. [Colorful Wood Protection Film](#protection)

3. [Assembly](#assembly) 

|


# 1. Assembly Parts

## 1.1. Metal Parts <a name="metalparts">



|![](Parts_Metal_1.jpg)|![](Parts_Metal_2.jpg)|![](Parts_Metal_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Threaded rod M8 x 180 mm |4 x M8 Nuts|2 x M8 Nuts with Rubber|
|![](Parts_Metal_4.jpg)|![](Parts_Metal_5.jpg)|
|4 x M6 Nuts |4 x M6 Cylinder Screw |
|![](Parts_Metal_6.jpg)|![](Parts_Metal_7.jpg)|![](Parts_Metal_8.jpg)|
|8 x W60x4 Wood Screw|4 x W35x4 Wood Screw|16 x W12x4 Wood Screw|


|

## 1.2. 3D Print Parts <a name="printparts">


|![](Parts_Print_1.jpg)|![](Parts_Print_2.jpg)|![](Parts_Print_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Ratchet Wheel|2 x Clamp Ring|5 x Comb Module|
|![](Parts_Print_4.jpg)|![](Parts_Print_5.jpg)|![](Parts_Print_6.jpg)|
|2 x Ratchet Pawl| 12 x Screw Socket w| 4 x Screw Socket w/o  |
|![](Parts_Print_7.jpg)|
|2 x Outer Warpcloth Beam (4 parts each)|


|

## 1.3. CNC Parts (Wood) <a name="cncparts">

|![](Parts_CNC_1.jpg)|![](Parts_CNC_2.jpg)|![](Parts_CNC_3.jpg)|
|:---------:|:------------:|:---:|
|2 x Comb Holder|2 x Side Frame|2 x Cross Beam|
|![](Parts_CNC_4.jpg)|![](Parts_CNC_5.jpg)|![](Parts_CNC_6.jpg)|
|2 x Slot Beam|2 x String Stick|1 x Shuttle|


|

## 1.4. Other Parts <a name="otherparts">

|![](Parts_Other_1.jpg)|![](Parts_Other_2.jpg)|
|:---------:|:------------:|
|2 x Inner Warpcloth Beam| ca. 3 m String|

|

# 2. Assembly Preparation

## 2.1. Colorful Wood Protection Film <a name="protection">


|![](Assembly_Preparation_1.jpg)|
|:---------:|
| 1. You will need color pigments, line oil, an empty glass, and a Brush|
|![](Assembly_Preparation_2.jpg)|
| 2. First, fill in the pigments (ca. 10-20 grams).|
|![](Assembly_Preparation_3.jpg)|
| 3. Next poor 8-10 times as much oil as pigments into the glass.|
|![](Assembly_Preparation_4.jpg)|
| 4. Stir it well, until you have got a smooth equally colored liquid without spots.|
|![](Assembly_Preparation_5.jpg)|
| 5. Make sure the wooden surface is clean and dry.|
| 6. Apply 1-3 layer, till you have got the wanted look.|
| 7. Let it dry for several hours (or better, leave it overnight).|

|

# 3. Assembly <a name="assembly">


|![](Assembly_1.jpg)![](Assembly_2.jpg)|
|:---------:|
| 1.|
|![](Assembly_3.jpg)![](Assembly_4.jpg)|
| 2.|
|![](Assembly_5.jpg)![](Assembly_6.jpg)|
| 3.|
|![](Assembly_7.jpg)![](Assembly_8.jpg)|
| 4.|
|![](Assembly_9.jpg)![](Assembly_10.jpg)|
| 5.|
|![](Assembly_11.jpg)![](Assembly_12.jpg)|
| 6.|
|![](Assembly_13.jpg)![](Assembly_14.jpg)![](Assembly_15.jpg)|
| 7.|
|![](Assembly_16.jpg)![](Assembly_17.jpg)![](Assembly_18.jpg)|
| 8.|
|![](Assembly_19.jpg)![](Assembly_20.jpg)![](Assembly_21.jpg)|
| 9.|
|![](Assembly_22.jpg)![](Assembly_23.jpg)|
| 10.|
|![](Assembly_24.jpg)|
| 11.|
|![](Assembly_25.jpg)|
| Voila! You just finished assembling your first OHLOOM. Congrats! |