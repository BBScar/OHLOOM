/*File Info--------------------------------------------------------------------
File Name: RatchetWheel.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


height=45;
cb_height=30;
cb_in=37;
cb_round=4;
cb_out=56;
gb_height=15;
gb_out=76;
hole=6.2;
tooth=gb_out-cb_out;
freecad_export = false;

module polar_array(radius,count,axis)
{
    for(i=[0:360/count:360])
    {
        rotate([0,0,i])
        translate([radius,0,0])
        children();
    }
}

if(freecad_export)
{
    //mirror([0,0,1])
    difference()
    {
        union()
        {
            translate([0,0,gb_height+cb_height-cb_round])
            cylinder(cb_round,d=cb_out-cb_round);
            translate([0,0,cb_round/2])
            rotate_extrude()
            translate([cb_out/2-cb_round/2,gb_height+cb_height-cb_round,0])
            circle(d=cb_round,$fn=12);
            translate([0,0,gb_height-cb_round/2])
            cylinder(cb_height,d=cb_out);
            cylinder(gb_height,d=gb_out);
            polar_array(0,18)
            difference()
            {
                translate([-gb_out/2+1,tooth/4,gb_height/2])
                rotate([0,0,-30])
                cylinder(gb_height,d=tooth,center=true,$fn=3);
                translate([-gb_out/2-16,0,gb_height/2])
                rotate([0,0,-35])
                cube([gb_height,gb_height*3,gb_height],center=true);
            }
          
        }
        translate([0,0,height/2])
        cylinder(height,d=cb_in,center=true);
        translate([0,0,gb_height+cb_height/2])
        rotate([0,90,0])
        cylinder(gb_out,d=hole,center=true);
    }
}
else
{
    //mirror([0,0,1])
    difference()
    {
        union()
        {
            translate([0,0,gb_height+cb_height-cb_round])
            cylinder(cb_round,d=cb_out-cb_round,$fn=80);
            translate([0,0,cb_round/2])
            rotate_extrude($fn=80)
            translate([cb_out/2-cb_round/2,gb_height+cb_height-cb_round,0])
            circle(d=cb_round,$fn=20);
            translate([0,0,gb_height-cb_round/2])
            cylinder(cb_height,d=cb_out,$fn=80);
            cylinder(gb_height,d=gb_out,$fn=80);
            polar_array(0,18)
            difference()
            {
                translate([-gb_out/2+1,tooth/4,gb_height/2])
                rotate([0,0,-30])
                cylinder(gb_height,d=tooth,center=true,$fn=3);
                translate([-gb_out/2-16,0,gb_height/2])
                rotate([0,0,-35])
                cube([gb_height,gb_height*3,gb_height],center=true);
            }
          
        }
        translate([0,0,height/2])
        cylinder(height,d=cb_in,center=true,$fn=80);
        translate([0,0,gb_height+cb_height/2])
        rotate([0,90,0])
        cylinder(gb_out,d=hole,center=true,$fn=80);
    }
}
